
#include <iostream>
#include <vector>
#include <cstring>

#include "exo.h"

using namespace std;

float add(float var1, float var2) {
    return var1 + var2;
}

float moins(float var1, float var2) {
    return var1 - var2;
}

float multiply(float var1, float var2) {
    return var1 * var2;
}

float divide(float var1, float var2) {
    return var1 / var2;
}

bool isPair(int var) {
    return (var % 2 == 0);
}

void minMax(int i, int j, int *min, int *max) {
    if (i < j) {
        *min = i;
        *max = j;
    } else {
        *min = j;
        *max = i;
    }

}

void swap(int a, int b, int *var1, int *var2) {

    *var1 = b;
    *var2 = a;
    a = *var1;
    b = *var2;

    cout << "avant échange" << endl;
    cout << "premier : " << a << endl;
    cout << "second : " << b << endl;
    cout << "après échange" << endl;
    cout << "premier : " << a << endl;
    cout << "second : " << b << endl;


}

void minMaxArray() {
    int *u, *o;
    int size;
    cout << "entrer une taille de tableau" << endl;
    cin >> size;
    int Tab[size];
    for (int i = 0; i < size; i++) {
        cout << "entrer la valeur " << (i + 1) << " tableau" << endl;
        cin >> Tab[i];
    }
}

/**
void minMaxArray(int *array, int size, int* min, int* max){

}
**/

int nba(char *p) {
    /** à mettre dans main

       char ss[50];
       cout << "Saisissez une chaine de caractères : " << endl;
       cin.getline(ss, 50);
       cout << "Vous avez saisi : " << ss << endl;
       cout << "La chaine comporte " << nba(ss) << " A." << endl;

    **/
    int n = 0;
    while (*p != '\0') {
        if (*p == 'A') n++;
        p++;
    }
    return n;
}

void TabInfini() {
    bool debFin{true};
    int size;
    int Saisi;
    string rep{"oui"};
    int res;
    std::vector<int> Tab;
    while (debFin != false) {
        cout << "combien de valeur voulez vous entrer: ";
        cin >> size;

        for (int i = 0; i < size; i++) {
            cout << "valeur " << (i + 1) << " " << endl;
            cin >> Saisi;
            Tab.push_back(Saisi);
        }

        for (int i = 0; i < size; i++) {
            res += Tab[i];
        }
        res = res * size / 100;
        cout << "la moyenne est de " << res << endl;

        cout << "voulez vous entrer d'autre valeur: " << endl;
        getline(cin, rep);

        if (rep == "oui") {
            debFin = true;
            continue;
        } else if (rep == "non") {
            debFin = false;
        }
    }
/** correction trouver au pif
* int *t;
 int i;
 t = (int *) malloc( 5 * sizeof(int) );
 if (t==NULL)
 cout << "pas assez de mémoire" << endl;
 else
 {
 for(i=0 ; i<5 ; i++)
 t[i] = i * i;
 for(i=0 ; i<5 ; i++)
 cout << t[i] << " ";
 cout << endl;
 free(t);
 }
 t = (int *) malloc( 10 * sizeof(int) );
 if (t==NULL)
 cout << "pas assez de mémoire" << endl;
 else
 {
 for(i=0;i<10;i++)
 t[i] = i * i;
 for(i=0 ; i<10 ; i++)
 cout << t[i] << " ";
 cout << endl;
 free(t);
 }
*/
}

struct Person {
    int age;
    char sexe;
    string name;
    float height;

};

/**
 * in main for struct
 * struct Person first, second;

    first.name = "Adam";

    cout << first.name << endl;
 */


void copy(char * ch1,char **ch2){
    char*   p1 = ch1;
    char*   p2;
    int     nb{0};

    while(*p1!='\0') {
        nb++;
        p1++; // pointeur vers le caractère suivant
    }

    *ch2 = (char *) malloc(sizeof(char) * nb);
    p2 = *ch2;
    p1 = ch1;
    while(*p1!='\0')
    {
        *p2 = *p1;
        cout << *p1 << endl;
        p1++; // pointeur vers le caractère suivant
        p2++; // pointeur vers le caractère suivant
    }

    /**
     int main() {
    char* ch1 = (char *) malloc(sizeof(char) * 100);
    char* ch2;

    cout << "Tapez une chaîne : " << endl;
    cin >> ch1;

    copy(ch1, &ch2);

    char* p1 = ch1;
    char* p2 = ch2;

    cout << "ch1 : ";
    while(*p1!='\0')
    {
        cout << *p1;
        p1++; // pointeur vers le caractère suivant
    }
    cout << endl << "ch2 : ";
    while(*p2!='\0')
    {
        cout << *p2;
        p2++; // pointeur vers le caractère suivant
    }
    cout << endl;
    free(ch1);
    free(ch2);
}
     * **/

}

void copy(char * ch1,char *&ch2) {
    char *p1 = ch1;
    char *p2;
    int nb{0};

    while (*p1 != '\0') {
        nb++;
        p1++; // pointeur vers le caractère suivant
    }

    ch2 = (char *) malloc(sizeof(char) * nb);
    p2 = ch2;
    p1 = ch1;
    while (*p1 != '\0') {
        *p2 = *p1;
        cout << *p1 << endl;
        p1++; // pointeur vers le caractère suivant
        p2++; // pointeur vers le caractère suivant
    }

    /****/

    void main() {
        char *ch1 = (char *) malloc(sizeof(char) * 100);
        char *ch2;

        cout << "Tapez une chaîne : " << endl;
        cin >> ch1;

        copy(ch1, ch2);

        char *p1 = ch1;
        char *p2 = ch2;

        cout << "ch1 : ";
        while (*p1 != '\0') {
            cout << *p1;
            p1++; // pointeur vers le caractère suivant
        }
        cout << endl << "ch2 : ";
        while (*p2 != '\0') {
            cout << *p2;
            p2++; // pointeur vers le caractère suivant
        }
        cout << endl;
        free(ch1);
        free(ch2);
    }


}
