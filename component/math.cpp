#include "math.h"

float pow(float number,int power){
    float initial {number};
    if (power > 0) {
        for(int i=1; i < power;i++){
            number *= number;
        }
        return number;
    } else if (power < 0){
        for(int i=1; i < power;i--){
            number *= number;
        }
        return 1/number;
    } else{
        return 1;
    }
}

float powRef(float& number,int power){
    float initialVal {number};
    if (power > 0) {
        for(int i=1; i < power;i++){
            number *= number;
        }
        return number;
    } else if (power < 0){
        for(int i=1; i < power;i--){
            number *= number;
        }
        return 1/number;
    } else{
        return 1;
    }
}

