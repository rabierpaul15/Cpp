//
// Created by paulf on 05/01/2021.
//

#ifndef COURC___EXO_H
#define COURC___EXO_H

float add(float var1, float var2);
float moins(float var1, float var2);
float multiply(float var1, float var2);
float divide(float var1, float var2);
bool isPair(int var);
void swap(int a,int b,int* var1,int* var2);
void minMaxArray();
int nba(char *p);
struct Person;
void copy(char * ch1, char * * ch2);


#endif //COURC___EXO_H
